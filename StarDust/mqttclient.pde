import mqtt.*;

StringList topicList;
StringList nodeList;
StringList serviceTypes;

XML database;
String networkName = "stardust";

String input = "sensor";
String output = "effector";
static final byte nodeNameIndex  = 0;
static final byte IOIndex        = 1;
static final byte serviceIndex   = 2;

void initMQTT() {
  client = new MQTTClient(this);
  database = new XML(networkName);
  topicList = new StringList(); // TODO: save and load topic list
  nodeList = new StringList();
  serviceTypes = new StringList();
  serviceTypes.append(input);
  serviceTypes.append(output);
  nodeList.append(networkName);
}

void connectMQTT(String toThisServer){
    client.connect("mqtt://"+toThisServer, networkName);
    client.subscribe("#"); // subscribe to all topics
}

boolean newTopic(String topic){
  boolean isNew = false;
  if (!topicList.hasValue(topic) == true) {
    isNew = true;
  }
  return isNew;
}

boolean newNode(String nodeName){
  boolean isNew = false;
  if (!nodeList.hasValue(nodeName) == true) {
    isNew = true;
  }
  return isNew;
}

void messageReceived(String topic, byte[] payload) {

  String[] backtrace = split(topic, '/');

  // filtering messages to stardust protocol: length and compatible service check
  if (backtrace.length >= serviceIndex && serviceTypes.hasValue(backtrace[IOIndex])) {

    if (!topicList.hasValue(topic)) {
      topicList.append(topic);
      println("Registered " + backtrace[IOIndex] + " : " + backtrace[serviceIndex] + " @ " + backtrace[nodeNameIndex]);
      redrawGraph();
      if (!nodeList.hasValue(backtrace[nodeNameIndex]))
      { // new node
        nodeList.append(backtrace[nodeNameIndex]); // add to the checklist
        int currentNodeIndex = nodeList.size()-1;
        nodes[currentNodeIndex] = new Dust(backtrace[nodeNameIndex]); // instantiate object
        if (backtrace[IOIndex].equals(input)) { nodes[currentNodeIndex].sensors.append(backtrace[serviceIndex]); }
        if (backtrace[IOIndex].equals(output)) { nodes[currentNodeIndex].effectors.append(backtrace[serviceIndex]); }
      }
      else
      { // not new node, but new service
        for (int i = 0; i < nodeList.size(); i++){
          if (nodeList.get(i).equals(backtrace[nodeNameIndex])) {
            if (backtrace[IOIndex].equals(input)) { nodes[i].sensors.append(backtrace[serviceIndex]); }
            if (backtrace[IOIndex].equals(output)) { nodes[i].effectors.append(backtrace[serviceIndex]); }
          }
        }
      }
    }
  }
}




// TODO: only save to XML on runtime database lists need
// database.addChild(decomposedTopic[nodeNameIndex]);
// database.getChild(decomposedTopic[nodeNameIndex]).addChild(decomposedTopic[IOIndex]);
// database.getChild(decomposedTopic[nodeNameIndex]).getChild(decomposedTopic[IOIndex]).addChild(decomposedTopic[serviceIndex]);
