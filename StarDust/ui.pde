import muehlseife.correlations.*;

Graph graph;

import 		controlP5.*;
ControlP5 cp5;

Textarea 	theConsole;
Textarea 	helpScreen;
Textfield brokerAddress;
Println 	console;
Button 		connectButton;

void initUI(){

	graph = new Graph(this);
	graph.showLabels = true;
	graph.fixColor = true;
	graph.setBoundingBox(0,0,width-200,height);
	graph.runPhysics = true;

	cursor(HAND);

  cp5 = new ControlP5(this);
  cp5.enableShortcuts();
	cp5.setColorActive(0x80ff0000);
	cp5.setColorBackground(color(0, 100));
	cp5.setColorForeground(color(0, 100));
	cp5.setColorLabel(0xffffffff);
	cp5.setColorValue(0xbbffffff);
	cp5.setFont(createFont("Monaco", 10, false));

	brokerAddress = cp5.addTextfield("Broker Address")
	.setPosition(width-350, 60)
	.setSize(200, 20)
	.setText("oberon.online:8883");

	connectButton = cp5.addButton("connect")
	.setPosition(width-110, 60)
	.setSize(60, 20);

	theConsole = cp5.addTextarea("console")
	.setFont(createFont("", 10))
	.setLineHeight(14)
	.setPosition(width-350, 120)
	.setSize(300, height-120-50)
	.setColor(color(200))
	.setColorBackground(color(0, 100))
	.setColorForeground(color(255, 100));

	console = cp5.addConsole(theConsole);

	helpScreen = cp5.addTextarea("help")
	.setFont(createFont("", 10))
	.setLineHeight(14)
	.setPosition(width/4, height/4)
	.setSize(width/3, height/2)
	.setColor(color(200))
	.setColorBackground(color(255,50,50, 100))
	.setColorForeground(color(255, 100))
	.setFont(createFont("Menlo",12,false))
	.setVisible(false);
}


void controlEvent(ControlEvent theEvent) {
	if ( theEvent.isFrom(connectButton) ) {
		connectMQTT(brokerAddress.getText());
	}
}


void redrawGraph(){
	for (int i = 1; i < nodeList.size(); i++){
		graph.put(nodes[i].name, new Node(width/2+random(50), height/2+random(50), 35, 70, color(255)));
		//println(nodes[i].name+" --- "+nodes[i].sensors);
		//		println(nodes[i].name+" --- "+nodes[i].effectors);
	}
}


void help(boolean on){
	helpScreen.setVisible(on);
	String lines[] = loadStrings("help.txt");
	String help = "";
	for (int i = 0; i < lines.length; i++){
		help += lines[i] + '\n';
	}
	helpScreen.setText(help);
}
