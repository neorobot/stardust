Methods	

getParent()	Gets a copy of the element's parent
getName()	Gets the element's full name
setName()	Sets the element's name
hasChildren()	Checks whether or not an element has any children
listChildren()	Returns the names of all children as an array
getChildren()	Returns an array containing all child elements
getChild()	Returns the child element with the specified index value or path
addChild()	Appends a new child to the element
removeChild()	Removes the specified child
getAttributeCount()	Counts the specified element's number of attributes
listAttributes()	Returns a list of names of all attributes as an array
hasAttribute()	Checks whether or not an element has the specified attribute
getString()	Gets the content of an attribute as a String
setString()	Sets the content of an attribute as a String
getInt()	Gets the content of an attribute as an int
setInt()	Sets the content of an attribute as an int
getFloat()	Gets the content of an attribute as a float
setFloat()	Sets the content of an attribute as a float
getContent()	Gets the content of an element
getIntContent()	Gets the content of an element as an int
getFloatContent()	Gets the content of an element as a float
setContent()	Sets the content of an element
format()	Formats XML data as a String
toString()	Gets XML data as a String using default formatting

Constructor	
XML(name)

Parameters	
name	String: creates a node with this name
Related	loadXML()
parseXML()
saveXML()