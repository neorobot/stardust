MQTTClient client;

public class Dust {
  public String name;
  public StringList sensors;
  public StringList effectors;

  Dust (String NewName){
    name = NewName;
    sensors = new StringList();
    effectors = new StringList();
  }

}

Dust[] nodes;

public void init() {
  frame.setTitle("StarDust Network");
  frame.removeNotify();
  //frame.setAlwaysOnTop(true);
  frame.setUndecorated(true);
  super.init();
}

void setup() {
  size(displayWidth, displayHeight);
  frameRate(50);
  initUI();
  initMQTT();
	console.clear();
  nodes = new Dust[16];
}

void draw() {
  background(84);
  graph.render();
}
