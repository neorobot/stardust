import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import mqtt.*; 
import muehlseife.correlations.*; 
import controlP5.*; 

import old.*; 
import muehlseife.correlations.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class StarDust extends PApplet {

MQTTClient client;

public class Dust {
  public String name;
  public StringList sensors;
  public StringList effectors;

  Dust (String NewName){
    name = NewName;
    sensors = new StringList();
    effectors = new StringList();
  }

}

Dust[] nodes;

public void init() {
  frame.setTitle("StarDust Network");
  frame.removeNotify();
  //frame.setAlwaysOnTop(true);
  frame.setUndecorated(true);
  super.init();
}

public void setup() {
  size(displayWidth, displayHeight);
  frameRate(50);
  initUI();
  initMQTT();
	console.clear();
  nodes = new Dust[16];
}

public void draw() {
  background(84);
  graph.render();
}

public void loadDatabase(){
	database = loadXML(networkName+".xml");
	println("Network loaded.");
	for ( int i = 0; i < database.listChildren().length; i++ ){
		if (database.getChild(i).getName().equals("#text")) {
			database.removeChild(database.getChild(i));
		}
	}
	println("Number of registered nodes: "+database.listChildren().length);
	println();
}

public void saveDatabase(){
	saveXML(database, networkName+".xml");
	println("Network saved.");
}
boolean helpVisible = false;

public void keyPressed(){

/*
  if (key == 's'){
    saveDatabase();
  }

  if (key == 'l'){
    loadDatabase();
  }

  if (key == 'h'){
    helpVisible = !helpVisible;
    help(helpVisible);
  }
*/

}

/*
import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public static byte [] float2ByteArray (float value)
{
     return ByteBuffer.allocate(4).putFloat(value).array();
}

static byte intentionIndex = 0;
static byte valueIndex = 1;
static byte minimumIndex = 5;
static byte maximumIndex = 9;
static byte payloadLength = 13;

public byte [] out(byte intention, float value, float minimum, float maximum){

byte [] o = new byte[payloadLength];

	o[intentionIndex] = intention;

	for (int i = valueIndex; i < minimumIndex; i++){
		o[i] = float2ByteArray(value)[i-valueIndex];
	}
	for (int i = minimumIndex; i < maximumIndex; i++){
		o[i] = float2ByteArray(minimum)[i-minimumIndex];
	}
	for (int i = maximumIndex; i < payloadLength; i++){
		o[i] = float2ByteArray(maximum)[i-maximumIndex];
	}

return o;
}
*/



//////////////////////////////////////////

public void mouseWheel(MouseEvent event) {
}

public void mouseDragged(){
}


StringList topicList;
StringList nodeList;
StringList serviceTypes;

XML database;
String networkName = "stardust";

String input = "sensor";
String output = "effector";
static final byte nodeNameIndex  = 0;
static final byte IOIndex        = 1;
static final byte serviceIndex   = 2;

public void initMQTT() {
  client = new MQTTClient(this);
  database = new XML(networkName);
  topicList = new StringList(); // TODO: save and load topic list
  nodeList = new StringList();
  serviceTypes = new StringList();
  serviceTypes.append(input);
  serviceTypes.append(output);
  nodeList.append(networkName);
}

public void connectMQTT(String toThisServer){
    client.connect("mqtt://"+toThisServer, networkName);
    client.subscribe("#"); // subscribe to all topics
}

public boolean newTopic(String topic){
  boolean isNew = false;
  if (!topicList.hasValue(topic) == true) {
    isNew = true;
  }
  return isNew;
}

public boolean newNode(String nodeName){
  boolean isNew = false;
  if (!nodeList.hasValue(nodeName) == true) {
    isNew = true;
  }
  return isNew;
}

public void messageReceived(String topic, byte[] payload) {

  String[] backtrace = split(topic, '/');

  // filtering messages to stardust protocol: length and compatible service check
  if (backtrace.length >= serviceIndex && serviceTypes.hasValue(backtrace[IOIndex])) {

    if (!topicList.hasValue(topic)) {
      topicList.append(topic);
      println("Registered " + backtrace[IOIndex] + " : " + backtrace[serviceIndex] + " @ " + backtrace[nodeNameIndex]);
      redrawGraph();
      if (!nodeList.hasValue(backtrace[nodeNameIndex]))
      { // new node
        nodeList.append(backtrace[nodeNameIndex]); // add to the checklist
        int currentNodeIndex = nodeList.size()-1;
        nodes[currentNodeIndex] = new Dust(backtrace[nodeNameIndex]); // instantiate object
        if (backtrace[IOIndex].equals(input)) { nodes[currentNodeIndex].sensors.append(backtrace[serviceIndex]); }
        if (backtrace[IOIndex].equals(output)) { nodes[currentNodeIndex].effectors.append(backtrace[serviceIndex]); }
      }
      else
      { // not new node, but new service
        for (int i = 0; i < nodeList.size(); i++){
          if (nodeList.get(i).equals(backtrace[nodeNameIndex])) {
            if (backtrace[IOIndex].equals(input)) { nodes[i].sensors.append(backtrace[serviceIndex]); }
            if (backtrace[IOIndex].equals(output)) { nodes[i].effectors.append(backtrace[serviceIndex]); }
          }
        }
      }
    }
  }
}




// TODO: only save to XML on runtime database lists need
// database.addChild(decomposedTopic[nodeNameIndex]);
// database.getChild(decomposedTopic[nodeNameIndex]).addChild(decomposedTopic[IOIndex]);
// database.getChild(decomposedTopic[nodeNameIndex]).getChild(decomposedTopic[IOIndex]).addChild(decomposedTopic[serviceIndex]);


Graph graph;


ControlP5 cp5;

Textarea 	theConsole;
Textarea 	helpScreen;
Textfield brokerAddress;
Println 	console;
Button 		connectButton;

public void initUI(){

	graph = new Graph(this);
	graph.showLabels = true;
	graph.fixColor = true;
	graph.setBoundingBox(0,0,width-200,height);
	graph.runPhysics = true;

	cursor(HAND);

  cp5 = new ControlP5(this);
  cp5.enableShortcuts();
	cp5.setColorActive(0x80ff0000);
	cp5.setColorBackground(color(0, 100));
	cp5.setColorForeground(color(0, 100));
	cp5.setColorLabel(0xffffffff);
	cp5.setColorValue(0xbbffffff);
	cp5.setFont(createFont("Monaco", 10, false));

	brokerAddress = cp5.addTextfield("Broker Address")
	.setPosition(width-350, 60)
	.setSize(200, 20)
	.setText("oberon.online:8883");

	connectButton = cp5.addButton("connect")
	.setPosition(width-110, 60)
	.setSize(60, 20);

	theConsole = cp5.addTextarea("console")
	.setFont(createFont("", 10))
	.setLineHeight(14)
	.setPosition(width-350, 120)
	.setSize(300, height-120-50)
	.setColor(color(200))
	.setColorBackground(color(0, 100))
	.setColorForeground(color(255, 100));

	console = cp5.addConsole(theConsole);

	helpScreen = cp5.addTextarea("help")
	.setFont(createFont("", 10))
	.setLineHeight(14)
	.setPosition(width/4, height/4)
	.setSize(width/3, height/2)
	.setColor(color(200))
	.setColorBackground(color(255,50,50, 100))
	.setColorForeground(color(255, 100))
	.setFont(createFont("Menlo",12,false))
	.setVisible(false);
}


public void controlEvent(ControlEvent theEvent) {
	if ( theEvent.isFrom(connectButton) ) {
		connectMQTT(brokerAddress.getText());
	}
}


public void redrawGraph(){
	for (int i = 1; i < nodeList.size(); i++){
		graph.put(nodes[i].name, new Node(width/2+random(50), height/2+random(50), 35, 70, color(255)));
		//println(nodes[i].name+" --- "+nodes[i].sensors);
		//		println(nodes[i].name+" --- "+nodes[i].effectors);
	}
}


public void help(boolean on){
	helpScreen.setVisible(on);
	String lines[] = loadStrings("help.txt");
	String help = "";
	for (int i = 0; i < lines.length; i++){
		help += lines[i] + '\n';
	}
	helpScreen.setText(help);
}
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "StarDust" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
