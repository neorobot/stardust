
void loadDatabase(){
	database = loadXML(networkName+".xml");
	println("Network loaded.");
	for ( int i = 0; i < database.listChildren().length; i++ ){
		if (database.getChild(i).getName().equals("#text")) {
			database.removeChild(database.getChild(i));
		}
	}
	println("Number of registered nodes: "+database.listChildren().length);
	println();
}

void saveDatabase(){
	saveXML(database, networkName+".xml");
	println("Network saved.");
}
