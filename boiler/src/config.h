#define HOSTNAME "DustConfig"

const int SONOFF_RELAY_PINS[4] =    {12, 12, 12, 12};

#define   SONOFF_AVAILABLE_CHANNELS 1
#define   SONOFF_BUTTON             0
#define   SONOFF_INPUT              14
#define   SONOFF_LED                13
#define   SONOFF_LED_RELAY_STATE    false

#define   DHT_PIN                   14
#define   DHT_TYPE                  22

#define   BOILER_STATUS             14
#define   BOILER_SET                4
#define   BOILER_RESET              5


#define     BOILER_ENABLED
//#define   DHT_ENABLED
//#define   AUREL_ENABLED
//#define   RELAY_ENABLED
