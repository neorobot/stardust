
#include <RCSwitch.h>
#include <config.h>
#include <WiFiManager.h>          //https://github.com/tzapu/WiFiManager
#include <PubSubClient.h>        //https://github.com/Imroy/pubsubclient
#include <ESP8266WiFi.h>
#include <EEPROM.h>
#include <Ticker.h>
#include <DHT.h>
#include <timer.h>

DHT dht(DHT_PIN, DHT_TYPE);
RCSwitch aurel = RCSwitch();
Timer sensorTimer, mqttTimer;
Ticker ticker;

WiFiClient wclient;
PubSubClient mqttClient(wclient);

static bool MQTT_ENABLED  = true;
bool mustRetry = true;

#define EEPROM_SALT 12661
typedef struct {
  char  bootState[4]      = "on";
  char  mqttHostname[33]  = "oberon.online";
  char  mqttPort[6]       = "8883";
  //char  mqttClientID[24]  = "generalNode";
  char  mqttTopic[33]     = "generalNode";
  char  sensorRefresh[6]  = "5000";
  int   salt              = EEPROM_SALT;
} WMSettings;

WMSettings settings;

const int CMD_WAIT = 0;
const int CMD_BUTTON_CHANGE = 1;

int cmd = CMD_WAIT;
int buttonState = HIGH;
static long startPress = 0;

int sensorRefresh = 5000;

void RFinit(){
#ifdef AUREL_ENABLED
  //aurel.enableTransmit(8);
  //aurel.setPulseLength(360);
  aurel.enableReceive(4);
#endif
}

// http://stackoverflow.com/questions/9072320/split-string-into-string-array
String getValue(String data, char separator, int index)
{
  int found = 0;
  int strIndex[] = {0, -1};
  int maxIndex = data.length()-1;

  for(int i=0; i<=maxIndex && found<=index; i++){
    if(data.charAt(i)==separator || i==maxIndex){
        found++;
        strIndex[0] = strIndex[1]+1;
        strIndex[1] = (i == maxIndex) ? i+1 : i;
    }
  }
  return found>index ? data.substring(strIndex[0], strIndex[1]) : "";
}

void tick()
{
  //toggle state
  int state = digitalRead(SONOFF_LED);  // get the current state of GPIO1 pin
  digitalWrite(SONOFF_LED, !state);     // set pin to the opposite state
}

//gets called when WiFiManager enters configuration mode
void configModeCallback (WiFiManager *myWiFiManager) {
  Serial.println("Entered config mode");
  Serial.println(WiFi.softAPIP());
  //if you used auto generated SSID, print it
  Serial.println(myWiFiManager->getConfigPortalSSID());
  //entered config mode, make led toggle faster
  ticker.attach(0.2, tick);
}

void updateRadio(long radiocode){
  #ifdef AUREL_ENABLED
  char topic[50];
  sprintf(topic, "%s/sensor/433", settings.mqttTopic);
  char value[10];
  ltoa(radiocode, value, 10);
  mqttClient.publish(topic, value);
  #endif
}

void updateRelay(int channel) {
  #ifdef RELAY_ENABLED
  int state = digitalRead(SONOFF_RELAY_PINS[channel]);
  char topic[50];
  sprintf(topic, "%s/sensor/channel-%d", settings.mqttTopic, channel);
  String stateString = state == 0 ? "off" : "on";
  if ( channel >= SONOFF_AVAILABLE_CHANNELS) {
    stateString = "disabled";
  }
  mqttClient.publish(topic, stateString);
  #endif
}

void updateHumidity() {
  #ifdef DHT_ENABLED
  float h = dht.readHumidity();
  if (h != NAN){
    char topic[50];
    sprintf(topic, "%s/sensor/humidity", settings.mqttTopic);
    char value[10];
    dtostrf( h, 2, 2, value );
    mqttClient.publish(topic, value);
  }
  #endif
}

void updateTemperature() {
  #ifdef DHT_ENABLED
  float t = dht.readTemperature();
  if (t != NAN){
    char topic[50];
    sprintf(topic, "%s/sensor/temperature", settings.mqttTopic);
    char value[10];
    dtostrf( t, 2, 2, value );
    mqttClient.publish(topic, value);
  }
  #endif
}

void setState(int state, int channel) {
  //relay
  digitalWrite(SONOFF_RELAY_PINS[channel], state);
  //led
  if (SONOFF_LED_RELAY_STATE) {
    digitalWrite(SONOFF_LED, (state + 1) % 2); // led is active low
  }
  //MQTT
  updateRelay(channel);
}

void turnOn(int channel = 0) {
  int relayState = HIGH;
  setState(relayState, channel);
}

void turnOff(int channel = 0) {
  int relayState = LOW;
  setState(relayState, channel);
}

void toggleState() {
  cmd = CMD_BUTTON_CHANGE;
}

//flag for saving data
bool shouldSaveConfig = false;

//callback notifying us of the need to save config
void saveConfigCallback () {
  Serial.println("Should save config");
  shouldSaveConfig = true;
}

void toggle(int channel = 0) {
  Serial.println("toggle state");
  Serial.println(digitalRead(SONOFF_RELAY_PINS[channel]));
  int relayState = digitalRead(SONOFF_RELAY_PINS[channel]) == HIGH ? LOW : HIGH;
  setState(relayState, channel);
}

void restart() {
  ESP.reset();
  delay(1000);
}

void reset() {
  setState(LOW, 0);
  WMSettings defaults;
  settings = defaults;
  EEPROM.begin(512);
  EEPROM.put(0, settings);
  EEPROM.end();
  //reset wifi credentials
  WiFi.disconnect();
  delay(1000);
  ESP.reset();
  delay(1000);
}


void mqttCallback(const MQTT::Publish& pub) {

  Serial.print(pub.topic());
  Serial.print(" => ");

  if (pub.has_stream()) {
    int BUFFER_SIZE = 100;
    uint8_t buf[BUFFER_SIZE];
    int read;
    while (read = pub.payload_stream()->read(buf, BUFFER_SIZE)) {
      Serial.write(buf, read);
    }
    pub.payload_stream()->stop();
    Serial.println("had buffer");
  }

  else {
    Serial.println(pub.payload_string());

    String topic = pub.topic();
    String payload = pub.payload_string();

    if (topic == settings.mqttTopic) {
      Serial.println("root message. ignoring.");
      return;
    }

    if (topic.startsWith(settings.mqttTopic)) {
      Serial.println("This node got message.");

      topic = topic.substring(strlen(settings.mqttTopic) + 1);
      String ioString = getValue(topic, '/', 0);
      Serial.print("ioString: "); Serial.println(ioString);
      String channelString = getValue(topic, '/', 1);
      Serial.print("chString: "); Serial.println(channelString);

      if(!ioString.startsWith("effector")) {
        Serial.println("no effector");
        return; // MEMO: just precaution until further commands implemented
      }

      // TODO: rename channel service to switch
      // TODO: add RCSwitch service -> fix lib 1st

      if (channelString.startsWith("channel")){
        channelString.replace("channel-", "");
        int channel = channelString.toInt();
        //Serial.println(channel);

        // payload
        if (payload == "on") { turnOn(channel); }
        if (payload == "off") { turnOff(channel); }
        if (payload == "toggle") { toggle(channel); }
        if (payload == "") { updateRelay(channel); }
      }
    }
  }
}

/// SETUP /////////////////////////////////////////////////////////////////

void setup()
{
  Serial.begin(115200);

  Serial.println("Booting...");

  RFinit();

  pinMode(SONOFF_LED, OUTPUT);
  // start ticker with 0.5 because we start in AP mode and try to connect
  ticker.attach(0.6, tick);

  const char *hostname = HOSTNAME;

  WiFiManager wifiManager;

  //set callback that gets called when connecting to previous WiFi fails, and enters Access Point mode
  wifiManager.setAPCallback(configModeCallback);

  //timeout - this will quit WiFiManager if it's not configured in 3 minutes, causing a restart
  wifiManager.setConfigPortalTimeout(180);

  //custom params
  EEPROM.begin(512);
  EEPROM.get(0, settings);
  EEPROM.end();

  if (settings.salt != EEPROM_SALT) {
    Serial.println("Invalid settings in EEPROM, trying with defaults");
    WMSettings defaults;
    settings = defaults;
  }

  //setup relay
  //TODO multiple relays
  pinMode(SONOFF_RELAY_PINS[0], OUTPUT);

   //TODO this should move to last state maybe
   //TODO multi channel support
  if (strcmp(settings.bootState, "on") == 0) {
    turnOn();
  } else {
    turnOff();
  }

  WiFiManagerParameter custom_boot_state("boot-state", "on/off on boot", settings.bootState, 33);
  wifiManager.addParameter(&custom_boot_state);

  Serial.println(settings.bootState);

  Serial.println(settings.mqttHostname);
  Serial.println(settings.mqttPort);
  //Serial.println(settings.mqttClientID);
  Serial.println(settings.mqttTopic);

  WiFiManagerParameter custom_mqtt_text("<br/>MQTT config. <br/> No url to disable.<br/>");
  wifiManager.addParameter(&custom_mqtt_text);

  WiFiManagerParameter custom_mqtt_hostname("mqtt-hostname", "Hostname", settings.mqttHostname, 33);
  wifiManager.addParameter(&custom_mqtt_hostname);

  WiFiManagerParameter custom_mqtt_port("mqtt-port", "port", settings.mqttPort, 6);
  wifiManager.addParameter(&custom_mqtt_port);

  //WiFiManagerParameter custom_mqtt_client_id("mqtt-client-id", "Client ID", settings.mqttTopic, 33);
  //wifiManager.addParameter(&custom_mqtt_client_id);

  WiFiManagerParameter custom_mqtt_topic("mqtt-topic", "NodeName", settings.mqttTopic, 33);
  wifiManager.addParameter(&custom_mqtt_topic);

  WiFiManagerParameter custom_sensor_rate("sensor-rate", "Refresh interval (ms))", settings.sensorRefresh, 6);
  wifiManager.addParameter(&custom_sensor_rate);

  //set config save notify callback
  wifiManager.setSaveConfigCallback(saveConfigCallback);

  if (!wifiManager.autoConnect(hostname)) {
    Serial.println("failed to connect and hit timeout");
    //reset and try again, or maybe put it to deep sleep
    ESP.reset();
    delay(1000);
  }

  if (shouldSaveConfig) {
    Serial.println("Saving config");

    strcpy(settings.bootState, custom_boot_state.getValue());

    strcpy(settings.mqttHostname, custom_mqtt_hostname.getValue());
    strcpy(settings.mqttPort, custom_mqtt_port.getValue());
    //strcpy(settings.mqttClientID, custom_mqtt_client_id.getValue());
    strcpy(settings.mqttTopic, custom_mqtt_topic.getValue());

    Serial.println(settings.bootState);

    EEPROM.begin(512);
    EEPROM.put(0, settings);
    EEPROM.end();
  }

  //config mqtt
  if (strlen(settings.mqttHostname) == 0) {
    MQTT_ENABLED = false;
  }
  if (MQTT_ENABLED) {
    mqttClient.set_server(settings.mqttHostname, atoi(settings.mqttPort));
  }

  //if you get here you have connected to the WiFi
  Serial.println("connected!");
  ticker.detach();

  //setup button
  pinMode(SONOFF_BUTTON, INPUT);
  attachInterrupt(SONOFF_BUTTON, toggleState, CHANGE);

  //setup led
  if (!SONOFF_LED_RELAY_STATE) {
    digitalWrite(SONOFF_LED, LOW);
  }
  Serial.println("done setup");

  #ifdef DHT_ENABLED
    dht.begin();
  #endif

  String sr = "";
  for (int i = 0; i < sizeof(settings.sensorRefresh); i++){
    sr += settings.sensorRefresh[i];
  }
  sensorRefresh = sr.toInt();

}

/// MAIN LOOP ///////////////////////////////////////////////////////////////
void loop()
{

  //mqtt loop
  if (MQTT_ENABLED) {
    if (!mqttClient.connected()) {
      if( mqttTimer.poll(5000) && mustRetry ) {
        Serial.println("Trying to connect to mqtt");
        if (mqttClient.connect(settings.mqttTopic)) {
          Serial.println("success");
          mqttClient.set_callback(mqttCallback);
          char topic[50];
          sprintf(topic, "%s/effector/+", settings.mqttTopic); // TODO: # multi level subsciption instead + single level
          Serial.print("Subscribing: ");
          mqttClient.subscribe(topic);
          Serial.println(topic);
          updateTemperature();
          updateHumidity();
          updateRelay(0);
          updateRadio(0);
          mustRetry = false;
        } else {
          Serial.println("failed");
          mustRetry = true;
        }
      }
    } else {
      mustRetry = true;
      mqttClient.loop();
    }
  }

  // 433 MHz loop
  if (aurel.available()) {

  if ( aurel.getReceivedValue() == 0) {
    Serial.print("Unknown encoding");
  } else {
    Serial.print("Received ");
    Serial.print( aurel.getReceivedValue() );
    Serial.print(" / ");
    Serial.print( aurel.getReceivedBitlength() );
    Serial.print("bit ");
    Serial.print("Protocol: ");
    Serial.println( aurel.getReceivedProtocol() );
    char topic[50];
    sprintf(topic, "%s/sensor/433", settings.mqttTopic);
    char value[10];
    ltoa(aurel.getReceivedValue(), value, 10);
    mqttClient.publish(topic, value);
  }

  aurel.resetAvailable();
}

  // BUTTON LOOP
  switch (cmd) {
    case CMD_WAIT:
      break;
    case CMD_BUTTON_CHANGE:
      int currentState = digitalRead(SONOFF_BUTTON);
      if (currentState != buttonState) {
        if (buttonState == LOW && currentState == HIGH) {
          long duration = millis() - startPress;
          if (duration < 1000) {
            Serial.println("[toggle]");
            toggle();
          } else if (duration < 5000) {
            Serial.println("[restart]");
            restart();
          } else if (duration < 60000) {
            Serial.println("[reset]");
            reset();
          }
        } else if (buttonState == HIGH && currentState == LOW) {
          startPress = millis();
        }
        buttonState = currentState;
      }
      break;
  }

  // SENSOR LOOP
  if (sensorTimer.poll(sensorRefresh)){
    updateTemperature();
    updateHumidity();
    updateRelay(0);
  }

}
