#include "Arduino.h"

#define BOILER_STATUS  14
#define BOILER_RESET   4
#define BOILER_SET     5

void setup(){
  pinMode(BOILER_STATUS, INPUT_PULLUP);
  pinMode(BOILER_RESET, OUTPUT);
  pinMode(BOILER_SET, OUTPUT);
  Serial.begin(9600);
  while (!Serial);
  Serial.println("Ready");
}

void boiler(bool state){
  if (state) {
    digitalWrite(BOILER_RESET, LOW);
    digitalWrite(BOILER_SET, HIGH);
  }
  else {
    digitalWrite(BOILER_RESET, HIGH);
    digitalWrite(BOILER_SET, LOW);
  }
}

bool state = true;
void loop(){

  state = !state;
  turn(state);
  delay(100);
  Serial.print(state);
  Serial.print(" : ");
  Serial.print(digitalRead(BOILER_STATUS));
  delay(900);

}
