import mqtt.*;

MQTTClient client;

static final byte nodeNameIndex  = 0;
static final byte IOIndex        = 1;
static final byte serviceIndex   = 2;

void setup(){
  client = new MQTTClient(this);
  client.connect("mqtt://oberon.online:8883", "radioRouter");
  client.subscribe("#");
}

void draw(){
}

void messageReceived(String topic, byte[] payload) {

  String[] backtrace = split(topic, '/');
  String content = String.valueOf(char(payload));

  if (backtrace[nodeNameIndex].equals("radio")){
    long longCode = Long.parseLong(content,10);
    int shortCode = int(longCode - 16760000);

    switch (shortCode){

      case 2196:
        println("A ON");
        client.publish("sofa/effector/channel-0","on");
      break;

      case 2193:
        println("A OFF");
        client.publish("sofa/effector/channel-0","off");
      break;

      case 5268:
        println("B ON");
        client.publish("desk/effector/channel-0","on");
      break;

      case 5265:
        println("B OFF");
        client.publish("desk/effector/channel-0","off");
      break;

      case 6036:
        println("C ON");
        client.publish("kitchen/effector/channel-0","on");
      break;

      case 6033:
        println("C OFF");
        client.publish("kitchen/effector/channel-0","off");
      break;

      case 6228:
        println("D ON");
        client.publish("bathroom/effector/channel-0","on");
      break;

      case 6225:
        println("D OFF");
        client.publish("bathroom/effector/channel-0","off");
      break;

      case 6276:
        println("E ON");
        client.publish("garderobe/effector/channel-0","on");
      break;

      case 6273:
        println("E OFF");
        client.publish("garderobe/effector/channel-0","off");
      break;

    }
  }
}


/*

16762196 // A ON
16762193 // A OFF
16765268 // B ON
16765265 // B OFF
16766036 // C ON
16766033 // C OFF
16766228 // D ON
16766225 // D OFF
16766276 // E ON
16766273 // E OFF

16766303 // released

*/
