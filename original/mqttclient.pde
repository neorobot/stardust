import mqtt.*;

MQTTClient client;
XML stardust;

/* ./Bob/sense/button/1 */

static final byte nodeNameIndex  = 1;
static final byte mainTopicIndex = 2;
static final byte serviceIndex   = 3;
static final byte serviceIDindex = 4;

String[] serviceList = {"act", "sense", "logic"}; // TODO: convert to enum
String[] commandList = {"sub", "unsub", "conn", "discon", "sync", "conf"};


void connectMQTT(String toThisServer){
    client.connect("mqtt://"+toThisServer, "GUI");
    client.subscribe("#"); // subscribe to all topics
}

void initMQTT() {

  client = new MQTTClient(this);
  //client.connect("mqtt://"+brokerAddress.getText(), "GUI");
  //client.subscribe("#"); // subscribe to all topics
  //TODO: check for existing xml, if there is any load it
  stardust = new XML("Dust");
  //stardust.addChild("GUI");
}


boolean newComer(String newcomer){
	boolean trulynew = true;
    for (int i = 0; i < stardust.listChildren().length; i++ ){
  	  if (newcomer.equals(stardust.listChildren()[i])) trulynew = false;
    }
	return trulynew;
}


boolean isService(String checkAgainst){
	boolean itis = false;
	for (int i = 0; i < serviceList.length; i++){
		if (checkAgainst.equals(serviceList[i])) itis = true;
	}
	return itis;
}

boolean serviceUnregistered(String thisChild, String thisService){
	boolean unregserv = false;
	if ( !stardust.getChild(thisChild).hasAttribute(thisService) ) unregserv = true;
	return unregserv;
}

boolean isSensorService(String thisService){
	boolean is = false;
		if ( thisService.equals(serviceList[1]) ) is = true;
	return is;
}

boolean isActuatorService(String thisService){
	boolean is = false;
		if ( thisService.equals(serviceList[0]) ) is = true;
	return is;
}

/*
	version proto)
	. collect nodes to a managed xml structure
	. ask for sync
	. collect services to the managed xml structure

	version next)
	. at sync, download node's own xml file
	. at config, upload modified xml to node
	. manage xml files in system as node representatives

*/


void messageReceived(String topic, byte[] payload) {

  print("["+topic+"]: ");
  String[] decomposedTopic = split(topic, '/');
  String nodeName = decomposedTopic[nodeNameIndex];

  if ( newComer(nodeName) ) {
	  stardust.addChild(nodeName);
 	  print(" new node |");
  }

  if ( decomposedTopic.length > mainTopicIndex ){
	  if ( isService(decomposedTopic[mainTopicIndex]) ) { // valid service topic
		  if ( decomposedTopic.length > serviceIndex ){ // and has a service id
			  String serviceAttribute = decomposedTopic[mainTopicIndex] + decomposedTopic[serviceIndex] + decomposedTopic[serviceIDindex];
			  if ( serviceUnregistered(nodeName, serviceAttribute) ){
				  stardust.getChild(nodeName).addChild(decomposedTopic[mainTopicIndex]);
				  stardust.getChild(nodeName).getChild(decomposedTopic[mainTopicIndex]).addChild(decomposedTopic[serviceIndex]);
				  stardust.getChild(nodeName).getChild(decomposedTopic[mainTopicIndex]).getChild(decomposedTopic[serviceIndex]).setInt("ID", int(decomposedTopic[serviceIDindex]));
				  stardust.getChild(nodeName).setString(serviceAttribute, "registered");
				  print(" new service | ");
			  }
		  }
	  }
  }


  String s = "";
  for (int i = 0; i < payload.length; i++){
	  s += char(payload[i]);
  }
  String[] decomposedPayload = split(s, ',');
  println(decomposedPayload);

}
