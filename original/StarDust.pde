

public void init() {

  frame.setTitle("StarDust Network");
  frame.removeNotify();
  //frame.setAlwaysOnTop(true);
  frame.setUndecorated(true);
  super.init();
}

void setup() {

  size(displayWidth, displayHeight);
  frameRate(50);
  initControls();
  initGraph();
  initMQTT();	  
  console.clear();
}

void draw() {

  background(84);
  network.render();
}
