void loadDatabase(){
	println("Loading database: ");
	stardust = loadXML("stardust.xml");
	
	for ( int i = 0; i < stardust.listChildren().length; i++ ){
		if (stardust.getChild(i).getName().equals("#text")) {
			stardust.removeChild(stardust.getChild(i));
			println("False node removed.");
		}
	}
	println("Number of registered nodes: "+stardust.listChildren().length);
	println();
	println("Testing service database: ");
	for ( int i = 0; i < stardust.listChildren().length; i++ ){
		for (int s = 0; s < stardust.getChild(i).listChildren().length; s++){
			
			if (!stardust.getChild(i).getChild(s).hasChildren()) {
				println("Removing: " + stardust.getChild(i).getChild(s).getName());
				stardust.getChild(i).removeChild(stardust.getChild(i).getChild(s));
			}
		}
	}		
	for ( int i = 0; i < stardust.listChildren().length; i++ ){
		for (int s = 0; s < stardust.getChild(i).listChildren().length; s++){
			
			if (stardust.getChild(i).getChild(s).hasChildren()) {
				println("Cleaning service: " + stardust.getChild(i).getChild(s).getName());
				for (int b = 0; b < stardust.getChild(i).getChild(s).listChildren().length; b++) {
					if (stardust.getChild(i).getChild(s).getChild(b).getName().equals("#text")){
						stardust.getChild(i).getChild(s).removeChild(stardust.getChild(i).getChild(s).getChild(b));
						println("Cleaned");
					}
				}
			}
		}
	}		
	
	println("");
	
	println("Network loaded.");
}

void saveDatabase(){
	saveXML(stardust, "stardust.xml");
	println("Network saved.");
}