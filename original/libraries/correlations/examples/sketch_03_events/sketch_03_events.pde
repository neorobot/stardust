import muehlseife.correlations.*;

Graph myGraph;
//Add an Edge by pressing ALT and dragging from start to target node!!!

void setup() {
  size(800, 600);
    
  myGraph = new Graph(this); //create a new Graph
  myGraph.fixColor = true;
  myGraph.registerCorrelationsEvent(this);
  myGraph.showLabels = true;
  
  //create 3 Nodes
  myGraph.put("node 1", new Node(100, 100, 35, 70, color(255, 125, 0)));
  myGraph.put("node 2", new Node(230, 160, 45, 90, color(230)));
  myGraph.put("node 3", new Node(360, 80, 35, 70, color(0, 125, 255)));

  //add edges between the nodes
  myGraph.get("node 2").addEdge(myGraph.get("node 3"), 1f);
  myGraph.get("node 2").addEdge(myGraph.get("node 1"), 1f);
  myGraph.get("node 1").addEdge(myGraph.get("node 2"), 1f);
  myGraph.get("node 1").addEdge(myGraph.get("node 3"), 0.7f).showSlider = true;
}
  
void draw() {
  background(255);
  myGraph.render();
}

void correlationsEvent(CorrelationsEvent event) {
  switch (event.getID()) {
    case CorrelationsEvent.EDGE_ADDED:
      Edge edge = event.getEdge();
      edge.setSlider(true, -1, 1, 0, 0);
      edge.setColor(new int[] {color(255, 0, 0), color(150), color(0)});
      edge.value = 0f;
    break;
  }
  
  println(event.toString());
}
