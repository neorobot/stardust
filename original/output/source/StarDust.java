import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import muehlseife.correlations.*; 
import java.nio.ByteBuffer; 
import java.nio.ByteOrder; 
import mqtt.*; 
import controlP5.*; 

import old.*; 
import muehlseife.correlations.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class StarDust extends PApplet {



public void init() {

  frame.setTitle("StarDust Network");
  frame.removeNotify();
  //frame.setAlwaysOnTop(true);
  frame.setUndecorated(true);
  super.init();
}

public void setup() {

  size(displayWidth, displayHeight);
  frameRate(50);
  initControls();
  initGraph();
  initMQTT();	  
  console.clear();
}

public void draw() {

  background(84);
  network.render();
}

Graph network;

int nodeSize, serviceSize, nodeField, serviceField;

static final int EDGE_MODIFIED = 202;

public void initGraph(){
    nodeSize = height/32;
    serviceSize = height/38;
    nodeField = nodeSize*5;
    serviceField = serviceSize*3;

    network = new Graph(this);
    network.registerCorrelationsEvent(this);
    network.fixColor   = true;
    network.fixSize	   = true;
    network.showLabels = true;

    network.setBoundingBox(0, 0, width-400, height); // space allowed for nodes

    network.collisionForce = 10.0f;
    network.attractionForce = 0.0f;
    network.relationsForce = 1.0f;
    network.runPhysics = true;
}

public void correlationsEvent(CorrelationsEvent event) {

	/*

		node.getPosition()
		node.move(x,y)

	*/


  switch (event.getID()) {

    case CorrelationsEvent.EDGE_ADDED:
      Edge edge = event.getEdge();
      edge.setSlider(true, -1, 1, 0, 0);
      edge.setColor(new int[] {color(255, 0, 0), color(127), color(0)});
      edge.value = 0f;
    break;

 	case CorrelationsEvent.SELECTION_ADDED:
		Node selected = event.getNode();
	break;

 	case EDGE_MODIFIED:
	    //println(event.toString());
		Edge b = event.getEdge();
		println(b.getOwner().caption + "->" + b.getTarget().caption + " = " + b.value);
	break;

  }

  //println(event.getID());
 // println(event.toString());
}

public void redrawGraph(){

	print("Network has " + stardust.listChildren().length + " nodes: ");
	println(stardust.listChildren());

	for ( int i = 0; i < stardust.listChildren().length; i++ ){ // cycle through all nodes

		float x = width/2+random(width/4);
		float y = height/2+random(height/4);

		print(stardust.listChildren()[i]);
		network.put( stardust.listChildren()[i], new Node(x, y, nodeSize, nodeField, nodeColor) );
		//TODO: save positions to xml

		if ( stardust.getChild(i).hasChildren()){ // if the node has services

			println(" has services:");
			XML nodeWithServices = stardust.getChild(i);

			XML sensorServices[] = nodeWithServices.getChildren("sensor");
			if (sensorServices.length > 0){
				println("Sensors: " + sensorServices.length);
				for (int s = 0; s < sensorServices.length; s++ ){
					for ( int b = 0; b < sensorServices[s].listChildren().length; b++ ){
						println(b +" : "+sensorServices[s].getChild(b).getName());
						String nameOfService = sensorServices[s].getChild(b).getName() +"-"+sensorServices[s].getChild(b).getInt("ID");
						network.put(nameOfService, new Node(x+random(5), y+random(5), serviceSize, serviceField, senseColor) );
					    network.get(nameOfService).addEdge( network.get(nodeWithServices.getName()), 0.5f ).showSlider = true;
					}
				}
			}

			XML actServices[] = nodeWithServices.getChildren("act");
			if (actServices.length > 0){
				println("Actuators: " + actServices.length);
				for (int s = 0; s < actServices.length; s++ ){
					for ( int b = 0; b < actServices[s].listChildren().length; b++ ){
						println(b);
						String nameOfService = actServices[s].getChild(b).getName() +"-"+actServices[s].getChild(b).getInt("ID");
						network.put(nameOfService, new Node(x+random(5), y+random(5), serviceSize, serviceField, actColor) );
					    network.get(nodeWithServices.getName()).addEdge(network.get(nameOfService), 0.5f).showSlider = true;
					    //network.get(nameOfService).fixSize = false;
					}
				}
			}

		println('\n');
		}
		else {
			println(" has no services");
		}
	} println("---"+'\n');
}
public void loadDatabase(){
	println("Loading database: ");
	stardust = loadXML("stardust.xml");
	
	for ( int i = 0; i < stardust.listChildren().length; i++ ){
		if (stardust.getChild(i).getName().equals("#text")) {
			stardust.removeChild(stardust.getChild(i));
			println("False node removed.");
		}
	}
	println("Number of registered nodes: "+stardust.listChildren().length);
	println();
	println("Testing service database: ");
	for ( int i = 0; i < stardust.listChildren().length; i++ ){
		for (int s = 0; s < stardust.getChild(i).listChildren().length; s++){
			
			if (!stardust.getChild(i).getChild(s).hasChildren()) {
				println("Removing: " + stardust.getChild(i).getChild(s).getName());
				stardust.getChild(i).removeChild(stardust.getChild(i).getChild(s));
			}
		}
	}		
	for ( int i = 0; i < stardust.listChildren().length; i++ ){
		for (int s = 0; s < stardust.getChild(i).listChildren().length; s++){
			
			if (stardust.getChild(i).getChild(s).hasChildren()) {
				println("Cleaning service: " + stardust.getChild(i).getChild(s).getName());
				for (int b = 0; b < stardust.getChild(i).getChild(s).listChildren().length; b++) {
					if (stardust.getChild(i).getChild(s).getChild(b).getName().equals("#text")){
						stardust.getChild(i).getChild(s).removeChild(stardust.getChild(i).getChild(s).getChild(b));
						println("Cleaned");
					}
				}
			}
		}
	}		
	
	println("");
	
	println("Network loaded.");
}

public void saveDatabase(){
	saveXML(stardust, "stardust.xml");
	println("Network saved.");
}
boolean helpVisible = false;
boolean lamp = false;




public static byte [] float2ByteArray (float value)
{
     return ByteBuffer.allocate(4).putFloat(value).array();
}

static byte intentionIndex = 0;
static byte valueIndex = 1;
static byte minimumIndex = 5;
static byte maximumIndex = 9;
static byte payloadLength = 13;

public byte [] out(byte intention, float value, float minimum, float maximum){

byte [] o = new byte[payloadLength];

	o[intentionIndex] = intention;

	for (int i = valueIndex; i < minimumIndex; i++){
		o[i] = float2ByteArray(value)[i-valueIndex];
	}
	for (int i = minimumIndex; i < maximumIndex; i++){
		o[i] = float2ByteArray(minimum)[i-minimumIndex];
	}
	for (int i = maximumIndex; i < payloadLength; i++){
		o[i] = float2ByteArray(maximum)[i-maximumIndex];
	}

return o;
}

public void keyPressed(){

	if (key == 'd'){
		client.publish("/kitchen/sense/button/221", ""); // payload: io,val,min,max
		client.publish("/kitchen/sense/temp/311", "");
		client.publish("/kitchen/act/relay/141", "");
		client.publish("/bedroom/sense/light/123", ""); // payload: io,val,min,max
		client.publish("/bedroom/sense/temp/153", "");
		client.publish("/bedroom/act/cooler/17", "");
		client.publish("/bedroom/act/heater/832", "");
		client.publish("/bath/sense/temp/41", "");
		client.publish("/bath/act/heater/234", "");
	}

	if (key == 'o'){

    //println(out(byte(1), 1.0f, 0.0f, 1.0f));
		byte [] test = {1,2,4,6,8,16,31,64,9,10,11,12,13};
		client.publish("/Garderobe/act/light/12", test);//out(byte(1), 1.0f, 0.0f, 1.0f));
	}

	if (key == 's'){
		saveDatabase();
	}

	if (key == 'l'){
		loadDatabase();
	}

	if (key == 'r'){
		redrawGraph();
	}

	if (key == 'h'){
		helpVisible = !helpVisible;
		help(helpVisible);
	}
	/////
}



//////////////////////////////////////////

public void mouseWheel(MouseEvent event) {
/*
  float e = event.getCount();
  float s = network.scale;
  network.scale = s+e*0.05f;
*/
}

public void mouseDragged(){
	//network.trans(new PVector(mouseX,mouseY)); // function trans(PVector) does not exist
}


MQTTClient client;
XML stardust;

/* ./Bob/sense/button/1 */

static final byte nodeNameIndex  = 1;
static final byte mainTopicIndex = 2;
static final byte serviceIndex   = 3;
static final byte serviceIDindex = 4;

String[] serviceList = {"act", "sense", "logic"}; // TODO: convert to enum
String[] commandList = {"sub", "unsub", "conn", "discon", "sync", "conf"};


public void connectMQTT(String toThisServer){
    client.connect("mqtt://"+toThisServer, "GUI");
    client.subscribe("#"); // subscribe to all topics
}

public void initMQTT() {

  client = new MQTTClient(this);
  //client.connect("mqtt://"+brokerAddress.getText(), "GUI");
  //client.subscribe("#"); // subscribe to all topics
  //TODO: check for existing xml, if there is any load it
  stardust = new XML("Dust");
  //stardust.addChild("GUI");
}


public boolean newComer(String newcomer){
	boolean trulynew = true;
    for (int i = 0; i < stardust.listChildren().length; i++ ){
  	  if (newcomer.equals(stardust.listChildren()[i])) trulynew = false;
    }
	return trulynew;
}


public boolean isService(String checkAgainst){
	boolean itis = false;
	for (int i = 0; i < serviceList.length; i++){
		if (checkAgainst.equals(serviceList[i])) itis = true;
	}
	return itis;
}

public boolean serviceUnregistered(String thisChild, String thisService){
	boolean unregserv = false;
	if ( !stardust.getChild(thisChild).hasAttribute(thisService) ) unregserv = true;
	return unregserv;
}

public boolean isSensorService(String thisService){
	boolean is = false;
		if ( thisService.equals(serviceList[1]) ) is = true;
	return is;
}

public boolean isActuatorService(String thisService){
	boolean is = false;
		if ( thisService.equals(serviceList[0]) ) is = true;
	return is;
}

/*
	version proto)
	. collect nodes to a managed xml structure
	. ask for sync
	. collect services to the managed xml structure

	version next)
	. at sync, download node's own xml file
	. at config, upload modified xml to node
	. manage xml files in system as node representatives

*/


public void messageReceived(String topic, byte[] payload) {

  print("["+topic+"]: ");
  String[] decomposedTopic = split(topic, '/');
  String nodeName = decomposedTopic[nodeNameIndex];

  if ( newComer(nodeName) ) {
	  stardust.addChild(nodeName);
 	  print(" new node |");
  }

  if ( decomposedTopic.length > mainTopicIndex ){
	  if ( isService(decomposedTopic[mainTopicIndex]) ) { // valid service topic
		  if ( decomposedTopic.length > serviceIndex ){ // and has a service id
			  String serviceAttribute = decomposedTopic[mainTopicIndex] + decomposedTopic[serviceIndex] + decomposedTopic[serviceIDindex];
			  if ( serviceUnregistered(nodeName, serviceAttribute) ){
				  stardust.getChild(nodeName).addChild(decomposedTopic[mainTopicIndex]);
				  stardust.getChild(nodeName).getChild(decomposedTopic[mainTopicIndex]).addChild(decomposedTopic[serviceIndex]);
				  stardust.getChild(nodeName).getChild(decomposedTopic[mainTopicIndex]).getChild(decomposedTopic[serviceIndex]).setInt("ID", PApplet.parseInt(decomposedTopic[serviceIDindex]));
				  stardust.getChild(nodeName).setString(serviceAttribute, "registered");
				  print(" new service | ");
			  }
		  }
	  }
  }


  String s = "";
  for (int i = 0; i < payload.length; i++){
	  s += PApplet.parseChar(payload[i]);
  }
  String[] decomposedPayload = split(s, ',');
  println(decomposedPayload);

}


ControlP5 cp5;

Textarea theConsole;
Textarea helpScreen;
Textfield brokerAddress;
Println console;
Button connectButton;

int nodeColor = color(255,255,255);
int actColor = color(255,127,0);
int senseColor = color(0,127,255);

public void initControls(){

	cursor(HAND);

    cp5 = new ControlP5(this);
    cp5.enableShortcuts();
	cp5.setColorActive(0x80ff0000);
	cp5.setColorBackground(color(0, 100));
	cp5.setColorForeground(color(0, 100));
//	cp5.setColorBackground(0xaa000000);
//	cp5.setColorForeground(0x32ffffff);
	cp5.setColorLabel(0xffffffff);
	cp5.setColorValue(0xbbffffff);
	cp5.setFont(createFont("Monaco", 10, false));

	brokerAddress = cp5.addTextfield("Broker Address")
        .setPosition(width-350, 60)
        .setSize(200, 20)
		.setText("192.168.2.2");

	connectButton = cp5.addButton("connect")
        .setPosition(width-100, 60)
    	.setSize(60, 20);

    theConsole = cp5.addTextarea("console")
		.setFont(createFont("", 10))
	    .setLineHeight(14)
		.setPosition(width-350, 120)
		.setSize(300, height-120-50)
		.setColor(color(200))
		.setColorBackground(color(0, 100))
		.setColorForeground(color(255, 100));

    console = cp5.addConsole(theConsole);

    helpScreen = cp5.addTextarea("help")
		.setFont(createFont("", 10))
	    .setLineHeight(14)
		.setPosition(width/4, height/4)
		.setSize(width/3, height/2)
		.setColor(color(200))
		.setColorBackground(color(255,50,50, 100))
		.setColorForeground(color(255, 100))
		.setFont(createFont("Menlo",12,false))
		.setVisible(false);

}


public void controlEvent(ControlEvent theEvent) {
	if ( theEvent.isFrom(connectButton) ) {
		connectMQTT(brokerAddress.getText());
	}
}


public void help(boolean on){
	helpScreen.setVisible(on);

	String lines[] = loadStrings("help.txt");
	String help = "";

	for (int i = 0; i < lines.length; i++){
		help += lines[i] + '\n';
	}

	helpScreen.setText(help);
}
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "StarDust" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
