
import controlP5.*;
ControlP5 cp5;

Textarea theConsole;
Textarea helpScreen;
Textfield brokerAddress;
Println console;
Button connectButton;

color nodeColor = color(255,255,255);
color actColor = color(255,127,0);
color senseColor = color(0,127,255);

void initControls(){

	cursor(HAND);

    cp5 = new ControlP5(this);
    cp5.enableShortcuts();
	cp5.setColorActive(0x80ff0000);
	cp5.setColorBackground(color(0, 100));
	cp5.setColorForeground(color(0, 100));
//	cp5.setColorBackground(0xaa000000);
//	cp5.setColorForeground(0x32ffffff);
	cp5.setColorLabel(0xffffffff);
	cp5.setColorValue(0xbbffffff);
	cp5.setFont(createFont("Monaco", 10, false));

	brokerAddress = cp5.addTextfield("Broker Address")
        .setPosition(width-350, 60)
        .setSize(200, 20)
		.setText("192.168.2.2");

	connectButton = cp5.addButton("connect")
        .setPosition(width-100, 60)
    	.setSize(60, 20);

    theConsole = cp5.addTextarea("console")
		.setFont(createFont("", 10))
	    .setLineHeight(14)
		.setPosition(width-350, 120)
		.setSize(300, height-120-50)
		.setColor(color(200))
		.setColorBackground(color(0, 100))
		.setColorForeground(color(255, 100));

    console = cp5.addConsole(theConsole);

    helpScreen = cp5.addTextarea("help")
		.setFont(createFont("", 10))
	    .setLineHeight(14)
		.setPosition(width/4, height/4)
		.setSize(width/3, height/2)
		.setColor(color(200))
		.setColorBackground(color(255,50,50, 100))
		.setColorForeground(color(255, 100))
		.setFont(createFont("Menlo",12,false))
		.setVisible(false);

}


void controlEvent(ControlEvent theEvent) {
	if ( theEvent.isFrom(connectButton) ) {
		connectMQTT(brokerAddress.getText());
	}
}


void help(boolean on){
	helpScreen.setVisible(on);

	String lines[] = loadStrings("help.txt");
	String help = "";

	for (int i = 0; i < lines.length; i++){
		help += lines[i] + '\n';
	}

	helpScreen.setText(help);
}
