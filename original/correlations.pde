import muehlseife.correlations.*;
Graph network;

int nodeSize, serviceSize, nodeField, serviceField;

static final int EDGE_MODIFIED = 202;

void initGraph(){
    nodeSize = height/32;
    serviceSize = height/38;
    nodeField = nodeSize*5;
    serviceField = serviceSize*3;

    network = new Graph(this);
    network.registerCorrelationsEvent(this);
    network.fixColor   = true;
    network.fixSize	   = true;
    network.showLabels = true;

    network.setBoundingBox(0, 0, width-400, height); // space allowed for nodes

    network.collisionForce = 10.0f;
    network.attractionForce = 0.0f;
    network.relationsForce = 1.0f;
    network.runPhysics = true;
}

void correlationsEvent(CorrelationsEvent event) {

	/*

		node.getPosition()
		node.move(x,y)

	*/


  switch (event.getID()) {

    case CorrelationsEvent.EDGE_ADDED:
      Edge edge = event.getEdge();
      edge.setSlider(true, -1, 1, 0, 0);
      edge.setColor(new int[] {color(255, 0, 0), color(127), color(0)});
      edge.value = 0f;
    break;

 	case CorrelationsEvent.SELECTION_ADDED:
		Node selected = event.getNode();
	break;

 	case EDGE_MODIFIED:
	    //println(event.toString());
		Edge b = event.getEdge();
		println(b.getOwner().caption + "->" + b.getTarget().caption + " = " + b.value);
	break;

  }

  //println(event.getID());
 // println(event.toString());
}

void redrawGraph(){

	print("Network has " + stardust.listChildren().length + " nodes: ");
	println(stardust.listChildren());

	for ( int i = 0; i < stardust.listChildren().length; i++ ){ // cycle through all nodes

		float x = width/2+random(width/4);
		float y = height/2+random(height/4);

		print(stardust.listChildren()[i]);
		network.put( stardust.listChildren()[i], new Node(x, y, nodeSize, nodeField, nodeColor) );
		//TODO: save positions to xml

		if ( stardust.getChild(i).hasChildren()){ // if the node has services

			println(" has services:");
			XML nodeWithServices = stardust.getChild(i);

			XML sensorServices[] = nodeWithServices.getChildren("sensor");
			if (sensorServices.length > 0){
				println("Sensors: " + sensorServices.length);
				for (int s = 0; s < sensorServices.length; s++ ){
					for ( int b = 0; b < sensorServices[s].listChildren().length; b++ ){
						println(b +" : "+sensorServices[s].getChild(b).getName());
						String nameOfService = sensorServices[s].getChild(b).getName() +"-"+sensorServices[s].getChild(b).getInt("ID");
						network.put(nameOfService, new Node(x+random(5), y+random(5), serviceSize, serviceField, senseColor) );
					    network.get(nameOfService).addEdge( network.get(nodeWithServices.getName()), 0.5f ).showSlider = true;
					}
				}
			}

			XML actServices[] = nodeWithServices.getChildren("act");
			if (actServices.length > 0){
				println("Actuators: " + actServices.length);
				for (int s = 0; s < actServices.length; s++ ){
					for ( int b = 0; b < actServices[s].listChildren().length; b++ ){
						println(b);
						String nameOfService = actServices[s].getChild(b).getName() +"-"+actServices[s].getChild(b).getInt("ID");
						network.put(nameOfService, new Node(x+random(5), y+random(5), serviceSize, serviceField, actColor) );
					    network.get(nodeWithServices.getName()).addEdge(network.get(nameOfService), 0.5f).showSlider = true;
					    //network.get(nameOfService).fixSize = false;
					}
				}
			}

		println('\n');
		}
		else {
			println(" has no services");
		}
	} println("---"+'\n');
}
