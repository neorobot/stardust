boolean helpVisible = false;
boolean lamp = false;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

public static byte [] float2ByteArray (float value)
{
     return ByteBuffer.allocate(4).putFloat(value).array();
}

static byte intentionIndex = 0;
static byte valueIndex = 1;
static byte minimumIndex = 5;
static byte maximumIndex = 9;
static byte payloadLength = 13;

public byte [] out(byte intention, float value, float minimum, float maximum){

byte [] o = new byte[payloadLength];

	o[intentionIndex] = intention;

	for (int i = valueIndex; i < minimumIndex; i++){
		o[i] = float2ByteArray(value)[i-valueIndex];
	}
	for (int i = minimumIndex; i < maximumIndex; i++){
		o[i] = float2ByteArray(minimum)[i-minimumIndex];
	}
	for (int i = maximumIndex; i < payloadLength; i++){
		o[i] = float2ByteArray(maximum)[i-maximumIndex];
	}

return o;
}

void keyPressed(){

	if (key == 'd'){
		client.publish("/kitchen/sense/button/221", ""); // payload: io,val,min,max
		client.publish("/kitchen/sense/temp/311", "");
		client.publish("/kitchen/act/relay/141", "");
		client.publish("/bedroom/sense/light/123", ""); // payload: io,val,min,max
		client.publish("/bedroom/sense/temp/153", "");
		client.publish("/bedroom/act/cooler/17", "");
		client.publish("/bedroom/act/heater/832", "");
		client.publish("/bath/sense/temp/41", "");
		client.publish("/bath/act/heater/234", "");
	}

	if (key == 'o'){

    //println(out(byte(1), 1.0f, 0.0f, 1.0f));
		byte [] test = {1,2,4,6,8,16,31,64,9,10,11,12,13};
		client.publish("/Garderobe/act/light/12", test);//out(byte(1), 1.0f, 0.0f, 1.0f));
	}

	if (key == 's'){
		saveDatabase();
	}

	if (key == 'l'){
		loadDatabase();
	}

	if (key == 'r'){
		redrawGraph();
	}

	if (key == 'h'){
		helpVisible = !helpVisible;
		help(helpVisible);
	}
	/////
}



//////////////////////////////////////////

void mouseWheel(MouseEvent event) {
/*
  float e = event.getCount();
  float s = network.scale;
  network.scale = s+e*0.05f;
*/
}

void mouseDragged(){
	//network.trans(new PVector(mouseX,mouseY)); // function trans(PVector) does not exist
}
